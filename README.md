Role
----

Deploy Openstack project and servers from PDF/IDF description files

BEWARE
------

we're following a "non master" branch for chained-ci-tools. Needs to
update when this branch will get merged.


Input
-----
  - configuration files:
    - mandatory:
      - vars/pdf.yml
      - vars/idf.yml
      - vars/openstack_openrc: the admin openrc for openstack creds
      - vars/cloud.yml: the admin cloud config
      - inventory/jumphost: the ansible inventory for the jumphost
  - Environment variables:
    - mandatory:
      - PRIVATE_TOKEN: to get the artifacts
      - artifacts_src or artifact_bin: the url to get the artifacts or
        the binary artifact
    - optional:
      - ADMIN:
          - role: trigg the admin part of the CI:
              - create_project
              - create_flavors
              - create_cloud
          - default: ""
      - CLEAN:
          - role: trigg the clean part of the CI
          - default: ""
      - USE_PRIVATE_IP:
          - role: does we use private or public ip in generated inventory
          - value type: boolean
          - default: False
      - TENANT_NAME:
          - role: the name of the tenant
          - value type: string
          - default: the value in idf (os_infra.tenant.name).
      - USER_NAME:
          - role: the name of the tenant
          - value type: string
          - default: the value in idf (os_infra.user.name).
      - IDENTIFIER:
          - role: string added to all created stuff
          - value type: string dns compatible
          - default: "-by-os-infra-manager"
        - NETWORK_IDENTIFIER:
          - role: string added to all network stuff
          - value type: string dns compatible. if set to NONE, no identifier put
          - **Warning**: if set to NONE and CLEAN set, we'll try to remove
            __all__ the networks of the project which are not set to external
          - default: value of identifier
      - PUBLIC_NET:
          - role: give the public net to connect to
          - default: if not given, will take the first external network found
      - ADD_FLOATING_IP:
          - role: to know if floating ip has to be added
          - value type: boolean
          - default: True
      - VM_HTTP_PROXY:
          - role: give HTTP_PROXY variable for the VM
          - default: None
      - VM_HTTPS_PROXY:
          - role: give HTTPS_PROXY variable for the VM
          - default: None
      - VM_NO_PROXY:
          - role: give NO_PROXY variable for the VM
          - default: None
Output
------
  - artifacts:
    - inventory/infra: the inventory of the deployed infra
    - vars/pdf.yml
    - vars/idf.yml
    - vars/user_cloud.yml: the user cloud config
    - vars/openstack_user_openrc: the user openrc
