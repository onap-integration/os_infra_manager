---
- name: get cloud config
  block:
    ##
    # Get clouds.yml if exists
    ##
    - name: ensure openstack config folder exists
      file:
        path: "{{ ansible_user_dir }}/.config/openstack"
        recurse: "yes"
        state: directory

    - name: wait for no lock
      stat:
        path:
          "{{ ansible_user_dir }}/.config/openstack/clouds.yaml.lock"
      register: lock
      until: lock.stat.exists == false
      retries: 12
      delay: 5

    - name: lock remote file
      copy:
        content: ""
        dest:
          "{{ ansible_user_dir }}/.config/openstack/clouds.yaml.lock"

    - name: check clouds.yaml exists
      stat:
        path: "{{ ansible_user_dir }}/.config/openstack/clouds.yaml"
      register: cloud_file

    - name: get cloud.yaml
      when: cloud_file.stat.exists == true
      block:
        - name: create temporary local file for cloud.yml
          tempfile:
            state: file
            suffix: temp
          register: tmp_cloud
          delegate_to: '127.0.0.1'

        - name: fetch cloud config
          fetch:
            dest: "{{ tmp_cloud.path }}"
            src: "{{ ansible_user_dir }}/.config/openstack/clouds.yaml"
            flat: "yes"

        - name: load cloud config
          include_vars:
            file: "{{ tmp_cloud.path }}"

      always:
        - name: destroy the local tmp_cloud
          file:
            path: "{{ tmp_cloud.path }}"
            state: absent
          delegate_to: '127.0.0.1'

    ##
    # Set cloud
    ##
    - name: set the cloud.yml content
      set_fact:
        user_cloud: >-
          {{ { os_username: {
                'region_name': os_region_name,
                'interface': os_interface,
                'auth': {
                  'username': os_username,
                  'password': os_password,
                  'project_name': os_project_name,
                  'auth_url': os_auth_url,
                }
              }
             }
          }}

    - name: add api version
      set_fact:
        user_cloud: "{{ user_cloud | combine({os_username: {
          'identity_api_version': os_identity_api_version} },
          recursive=True) }}"
      when: os_identity_api_version is defined and os_identity_api_version != ''

    - name: add compute api version
      set_fact:
        user_cloud: "{{ user_cloud | combine({os_username: {
          'compute_api_version': os_compute_api_version} }, recursive=True) }}"
      when: os_compute_api_version is defined and os_compute_api_version != ''

    - name: add project domain name
      set_fact:
        user_cloud: "{{ user_cloud | combine({os_username: {
          'project_domain_name': os_project_domain_name} }, recursive=True) }}"
      when: os_project_domain_name is defined and os_project_domain_name != ''

    - name: add user domain name
      set_fact:
        user_cloud: "{{ user_cloud | combine({os_username: {
          'auth':
            {'user_domain_name': os_user_domain_name} } }, recursive=True) }}"
      when: os_user_domain_name is defined and os_user_domain_name != ''

    - name: add specific ca certificate
      set_fact:
        user_cloud: "{{ user_cloud | combine({os_username: {
          'cacert': ansible_user_dir + '/.config/openstack/' +
          os_username + '_' + (os_cacert | basename) } },
          recursive=True) }}"
      when: os_cacert is defined and os_cacert != ''

    - name: copy specific ca certificates
      copy:
          dest:
            "{{ ansible_user_dir }}/.config/openstack/{{ os_username }}_{{ os_cacert|basename }}"
          src: "{{ playbook_dir }}/{{ os_cacert }}"
      when: os_cacert is defined and os_cacert != ''

    - name: add project id
      set_fact:
        user_cloud: "{{ user_cloud | combine({os_username: {
          'auth':
            {'project_id': os_project_id} } }, recursive=True) }}"
      when: os_project_id is defined and os_project_id != ''
    ##
    # Save auth file for the artifacts
    ##
    - name: save local user_cloud.yml
      copy:
        content: "{{ {'clouds': user_cloud } | to_nice_yaml }}"
        dest: "{{ local_cloud }}"
      when: local_cloud != "NONE"
      delegate_to: "127.0.0.1"

    - name: generate cloud config
      copy:
        content: >-
          {{ { 'clouds': clouds
                           | default({})
                           | combine( user_cloud ) }
             | to_nice_yaml }}
        dest: "{{ ansible_user_dir }}/.config/openstack/clouds.yaml"

  always:
      ##
      # CLEAN
      ##
    - name: remove the lock file
      file:
        path: "{{ ansible_user_dir }}/.config/openstack/clouds.yaml.lock"
        state: absent
