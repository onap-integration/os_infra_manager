---

- name: load facts
  include_tasks: get_facts.yml

- name: create server groups
  os_server_group:
    name: "{{ item }}{{ identifier }}"
    state: present
    policies:
      - soft-anti-affinity
    cloud: "{{ openstack_user_name }}"
  loop: "{{ roles|list }}"
  register: server_groups
#
- name: retrieve server group ids
  set_fact:
    groups_id:
      "{{ (groups_id|default({})) |
           combine(
            {server_groups.results[index].server_group.name:
              server_groups.results[index].server_group.id}) }}"
  loop: "{{ roles|list }}"
  loop_control:
    index_var: index

- name: manage volumes
  include_tasks: volume_creation.yaml
  loop: "{{ nodes }}"
  loop_control:
    loop_var: node
    label: "{{ node.name }}"

- name: create servers
  os_server:
    cloud: "{{ openstack_user_name }}"
    flavor:
      "{{ item.node.flavor |
          default('custom.' ~ item.node.cpus ~ '_cores_' ~ item.node.memory ~
                  '_RAM_' ~ item.disks[0].disk_capacity ~ '_disk') }}"
    image: >-
      {{ (item.node.model in images) |
         ternary( item.node.model, os_infra.image_default) }}
    name: "{{ item.name }}{{ identifier }}"
    nics: "{{ net_list }}"
    state: present
    key_name: "{{ openstack_user_name   | regex_replace('[@\\.]', '_')}}_runner"
    auto_ip: "no"
    userdata: |
      #cloud-config
      users:
        - default
      write_files:
        - content: |
            #!/usr/bin/env bash

            if [ ! -e /usr/bin/python ];
              then sudo ln -s /usr/bin/python3 /usr/bin/python;
            fi
          permissions: '0755'
          path: /root/right_python.sh
      runcmd:
        - /root/right_python.sh

    security_groups:
      - "{{ openstack_tenant_name }}{{ identifier }}"
    scheduler_hints:
      group:
        "{{ groups_id[(os_infra.nodes_roles[item.name]|first) ~ identifier] }}"
  loop: "{{ nodes }}"
  register: server
  loop_control:
    label: "{{item.name}}"

- name: add floating IP to VM
  os_floating_ip:
    cloud: "{{ openstack_user_name }}"
    server: "{{ item.name }}{{ identifier }}"
    state: present
    wait: "yes"
    reuse: "yes"
    network: "{{ public_net_name }}"
  with_items: "{{ nodes }}"
  register: floating
  loop_control:
    label: "{{item.name}}"
  when: add_floating_ip

- name: attach volumes
  include_tasks: volume_attachment.yaml
  loop: "{{ nodes }}"
  loop_control:
    loop_var: node
    label: "{{ node.name }}"

- name: generate empty facts
  set_fact:
    private_ips: {}
    public_ips: {}
    users: {}

- name: retrieve private ip ip address
  set_fact:
    private_ips:
      "{{ private_ips|combine({item.0.name: item.1.server.private_v4}) }}"
  with_together:
    - "{{ nodes }}"
    - "{{ server.results }}"
  loop_control:
    label: "{{item.0.name}}"

- name: retrieve floating ip ip address
  set_fact:
    public_ips:
      "{{public_ips|combine({item.0.name: item.1.floating_ip.floating_ip_address}) }}"
  with_together:
    - "{{ nodes }}"
    - "{{ floating.results }}"
  loop_control:
    label: "{{ item.0.name }}"
  when: add_floating_ip

- name: save right ip address
  set_fact:
    ips: "{{ (use_private_ip or (not add_floating_ip)) |
              ternary(private_ips, public_ips) }}"

- name: set default user
  set_fact:
    users: >-
      {{ users|combine( {item.name:
          os_infra.image2user_mapping[(item.node.model in images) |
             ternary( item.node.model, os_infra.image_default)]}) }}
  loop: "{{ nodes }}"
  loop_control:
    label: "{{ item.name }}"

- name: generate inventory
  template:
    src: inventory.j2
    dest: "{{ inventory_dir }}/infra"
  delegate_to: "127.0.0.1"
